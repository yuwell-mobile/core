package com.yuwell.androidbase.web.manager;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.yuwell.androidbase.view.inter.InterToolbarActivity;

import wendu.webviewjavascriptbridge.WVJBWebView;

/**
 * Created by Chen on 2019/12/4.
 */
public interface WVJBFragment {

    WVJBWebView getWVJBWebView();

    void setRefreshEnabled(boolean enabled);

    WVJBHandlerManager getHandlerManager();

    @Nullable FragmentActivity getFragmentActivity();

    InterToolbarActivity getToolbarActivity();
}
