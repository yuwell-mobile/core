package com.yuwell.androidbase.web;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.yuwell.androidbase.tool.FileManager;
import com.yuwell.androidbase.tool.ImageUtil;
import com.yuwell.androidbase.view.AppBaseFragment;
import com.yuwell.androidbase.view.inter.InterToolbarActivity;
import com.yuwell.androidbase.web.manager.WVJBFragment;
import com.yuwell.androidbase.web.manager.WVJBHandlerManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import wendu.webviewjavascriptbridge.WVJBWebView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Chen on 2016/3/28.
 */
@RuntimePermissions
public class WebViewFragment extends AppBaseFragment implements WVJBFragment {

    public static final int REQUEST_CAMERA = 10000;
    public static final int REQUEST_FROM_LIBRARY = 10001;
    public static final int REQUEST_FILE = 10002;

    private String url;
    private WVJBWebView mWebView;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout mRefreshLayout;

    private ValueCallback<Uri[]> mFilePathCallback;
    private ValueCallback<Uri> mUploadFile;

    private Uri mPhotoUri;

    private WVJBHandlerManager handlerManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        handlerManager = new WVJBHandlerManager(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CommonWebViewClient.DATA_URL, url);
    }

    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        url = getArguments() != null ? getArguments().getString(ArgsBuilder.URL) : "";
        return inflater.inflate(R.layout.layout_webpage, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mProgressBar = view.findViewById(R.id.pb_loading);
        mWebView = view.findViewById(R.id.webView);
        mRefreshLayout = view.findViewById(R.id.swipe_refresh);

        initWebView();
        initSwipeRefreshLayout();

        if (savedInstanceState != null) {
            url = savedInstanceState.getString(CommonWebViewClient.DATA_URL);
        }

        HashMap<String, String> headers = (HashMap<String, String>) getArguments().getSerializable(ArgsBuilder.HEADERS);
        if (headers != null) {
            mWebView.loadUrl(url, headers);
        } else {
            mWebView.loadUrl(url);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSaveFormData(false);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setAppCachePath(getContext().getCacheDir() + File.separator + "webcache");
        settings.setAppCacheEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        CommonWebChromeClient webChromeClient = new CommonWebChromeClient(getContext()) {

            // For Lollipop 5.0+ Devices
            @TargetApi(VERSION_CODES.LOLLIPOP)
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback,
                    WebChromeClient.FileChooserParams fileChooserParams) {
                mFilePathCallback = filePathCallback;
                String[] types = fileChooserParams.getAcceptTypes();
                if (types.length > 0) {
                    boolean image = false;
                    for (String type : types) {
                        if (type.contains("image")) {
                            image = true;
                            break;
                        }
                    }

                    if (image) {
                        showImageChooserDialog(mWebView, fileChooserParams.isCaptureEnabled());
                    } else {
                        Intent intent = fileChooserParams.createIntent();
                        startActivityForResult(intent, REQUEST_FILE);
                    }
                    return true;
                }
                return false;
            }
        };

        webChromeClient.setProgressBar(mProgressBar);
        mWebView.setWebChromeClient(webChromeClient);

        mWebView.setWebViewClient(new CommonWebViewClient(getContext()) {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return (overrideUrlLoading(view, url) || super.shouldOverrideUrlLoading(view, url));
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                mRefreshLayout.setRefreshing(false);
                onPageLoadError(view, errorCode, description, failingUrl);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mWebView.evaluateJavascript("if (yw.onBridgeLoaded) {yw.onBridgeLoaded();}");
                mRefreshLayout.setRefreshing(false);
                WebViewFragment.this.onPageFinished();
            }
        });

        onInitWebView(mWebView);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_FROM_LIBRARY:
                if (resultCode == RESULT_OK) {
                    compressImage(data.getData());
                } else {
                    onFileChosen(null);
                }
                break;
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                    compressImage(mPhotoUri);
                } else {
                    onFileChosen(null);
                }
                break;
            case REQUEST_FILE:
                if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
                    Uri[] uris = WebChromeClient.FileChooserParams.parseResult(resultCode, data);
                    onFileChosen(uris);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void onInitWebView(WVJBWebView mWebView) {
        handlerManager.init(mWebView);
    }

    protected boolean overrideUrlLoading(WebView view, String url) {
        return false;
    }

    protected void onPageFinished() {
        if (getToolbarActivity() != null) {
            getToolbarActivity().setTopTitle(getWVJBWebView().getTitle());
        }
    }

    protected void onPageLoadError(WebView view, int errorCode, String description, String failingUrl) {}

    private void showImageChooserDialog(WebView mWebView, boolean capture) {
        if (capture) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mWebView.getContext());
            builder.setTitle("选择图片");
            builder.setItems(new String[]{"相册", "相机"}, ((dialog, which) -> {
                if (which == 0) {
                    WebViewFragmentPermissionsDispatcher.chooseImageFromLibraryWithPermissionCheck(
                            WebViewFragment.this);
                } else {
                    WebViewFragmentPermissionsDispatcher.takePhotoWithPermissionCheck(
                            WebViewFragment.this);
                }
            }));
            builder.setOnCancelListener(dialog -> onFileChosen(null));
            builder.create().show();
        } else {
            WebViewFragmentPermissionsDispatcher.chooseImageFromLibraryWithPermissionCheck(
                    WebViewFragment.this);
        }
    }

    private void initSwipeRefreshLayout() {
        mRefreshLayout.setOnRefreshListener(this::refresh);
    }

    public void refresh() {
        if (getArguments() == null) {
            return;
        }

        String url = getArguments().getString(CommonWebViewClient.DATA_URL);
        if (!TextUtils.isEmpty(url)) {
            HashMap<String, String> headers = (HashMap<String, String>) getArguments().getSerializable(ArgsBuilder.HEADERS);
            if (headers != null) {
                mWebView.loadUrl(url, headers);
            } else {
                mWebView.loadUrl(url);
            }
        }
    }

    private void onFileChosen(Uri[] uris) {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(uris);
                mFilePathCallback = null;
            }
        }
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void takePhoto() {
        mPhotoUri = FileManager.getTempImageUri(getContext(), "FileProvider1");
        ImageUtil.takePhoto(this, REQUEST_CAMERA, mPhotoUri);
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    void chooseImageFromLibrary() {
        ImageUtil.chooseFromLib(this, REQUEST_FROM_LIBRARY);
    }

    private void compressImage(Uri uri) {
        String actual = ImageUtil.resolveImageUriToActual(getContext(), uri);
        File input = new File(actual);

        if (!input.exists()) {
            onFileChosen(null);
        }

        try {
            File out = ImageUtil.getCompressedFile(actual, FileManager.getTempDir(getContext()));
            if (out != null) {
                Uri outUri = FileManager.getFileUri(getContext(), out, "FileProvider1");
                onFileChosen(new Uri[] {outUri});
            } else {
                onFileChosen(null);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            onFileChosen(null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        WebViewFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public WVJBWebView getWVJBWebView() {
        return mWebView;
    }

    @Override
    public void setRefreshEnabled(boolean enabled) {
        mRefreshLayout.setEnabled(enabled);
    }

    @Override
    public WVJBHandlerManager getHandlerManager() {
        return handlerManager;
    }

    @Override
    public FragmentActivity getFragmentActivity() {
        return getActivity();
    }

    @Override
    public InterToolbarActivity getToolbarActivity() {
        if (getActivity() instanceof InterToolbarActivity) {
            return (InterToolbarActivity) getActivity();
        }
        return null;
    }
}
