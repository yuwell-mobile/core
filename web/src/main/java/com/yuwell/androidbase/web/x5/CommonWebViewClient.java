package com.yuwell.androidbase.web.x5;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.tencent.smtt.export.external.interfaces.WebResourceRequest;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Chen on 2016/4/19.
 */
public abstract class CommonWebViewClient extends WebViewClient {

    public static final String DATA_URL = "url";
    private static final String CATEGROY_PREFIX = "com.yuwell.browser";
    private static final String ACTION_START_ACTIVITY = ".action.START_ACTIVITY";

    private static final String ERROR_URL = "file:///android_asset/error.html";

    private Context context;

    private String loadedUrl;
    private String actionString;

    public CommonWebViewClient(Context context) {
        this.context = context;
        actionString = context.getPackageName() + ACTION_START_ACTIVITY;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // do your handling codes here, which url is the requested url
        // probably you need to open that url rather than redirect:
        Uri uri = Uri.parse(url);
        if (uri.getScheme().equals("webview")) {
            Intent intent = new Intent();
            intent.setAction(actionString);
            intent.addCategory(CATEGROY_PREFIX);
            if (!TextUtils.isEmpty(uri.getQueryParameter("url"))) {
                intent.putExtra(DATA_URL, uri.getQueryParameter("url"));
            } else {
                intent.putExtra(DATA_URL, url.replaceAll("webview://", ""));
            }
            if (!TextUtils.isEmpty(uri.getQueryParameter("uid"))) {
                intent.putExtra("uid", uri.getQueryParameter("uid"));
            }
            context.startActivity(intent);
            return true;
        } else {
            view.loadUrl(url);
            return false;
        }
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        redirectToErrorPage(view, errorCode, description, failingUrl);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (!TextUtils.isEmpty(loadedUrl) && !url.contains(ERROR_URL) && loadedUrl.equals(url)) {
            clearHistory(view);
        }
        loadedUrl = url;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        WebResourceResponse resourceResponse = getLocalResource(request.getUrl());
        if (resourceResponse != null) {
            return resourceResponse;
        } else {
            return super.shouldInterceptRequest(view, request);
        }
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        WebResourceResponse resourceResponse = getLocalResource(Uri.parse(url));
        if (resourceResponse != null) {
            return resourceResponse;
        } else {
            return super.shouldInterceptRequest(view, url);
        }
    }

    private WebResourceResponse getLocalResource(Uri uri) {
        if ("local".equals(uri.getScheme())) {
            InputStream is = null;
            try {
                is = context.getAssets().open(uri.getHost() + uri.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(uri.getLastPathSegment()));
            return new WebResourceResponse(mimeType, "utf-8", is);
        } else {
            return null;
        }
    }

    private void redirectToErrorPage(WebView view, int errorCode, String description, String failingUrl) {
        Log.d("WebView", errorCode + " " + description);
        try {
            view.loadUrl(ERROR_URL + "?url=" + URLEncoder.encode(failingUrl, "UTF-8"));
            clearHistory(view);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void clearHistory(final WebView view) {
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.clearHistory();
            }
        }, 500);
    }

}
