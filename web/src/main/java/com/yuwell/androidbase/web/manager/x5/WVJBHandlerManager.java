package com.yuwell.androidbase.web.manager.x5;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;

import com.yuwell.androidbase.tool.DensityUtil;
import com.yuwell.androidbase.tool.FileManager;
import com.yuwell.androidbase.tool.ImageUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import wendu.webviewjavascriptbridge.x5.WVJBWebView;

/**
 * Created by Chen on 2019/12/4.
 */
public class WVJBHandlerManager {

    private static final String TAG = "WVJBHandlerManager";

    public static final int REQUEST_FROM_LIBRARY = 1001;
    public static final int REQUEST_FROM_CAMERA = 1002;
    public static final int REQUEST_CODE_SCAN = 1003;

    private static ArrayMap<Long, String> idPathMap = new ArrayMap<>();

    private WVJBFragment fragment;

    private OkHttpClient client = new OkHttpClient();
    private JSONArray menuArray;
    private Uri mPhotoUri;

    private WVJBWebView.WVJBResponseCallback<JSONObject> imageCallback;
    private WVJBWebView.WVJBResponseCallback<JSONObject> scanCallback;

    public WVJBHandlerManager(WVJBFragment fragment) {
        this.fragment = fragment;
    }

    public void init(WVJBWebView webView) {
        webView.registerHandler("getImage", (WVJBWebView.WVJBHandler<Object, JSONObject>) (o, callback) -> {
            showChooseImgDialog((JSONArray) o);
            imageCallback = callback;
        });

        webView.registerHandler("showOptionMenu", (WVJBWebView.WVJBHandler<Object, JSONObject>) (o, callback) -> {
            Map<String, Object> kv = new HashMap<>();
            if (o instanceof JSONArray) {
                menuArray = (JSONArray) o;
                if (menuArray.length() > 0) {
                    if (fragment.getFragmentActivity() != null) {
                        fragment.getFragmentActivity().invalidateOptionsMenu();
                    }
                    kv.put("status", true);
                } else {
                    kv.put("status", false);
                }
            } else {
                kv.put("status", false);
            }
            callback.onResult(new JSONObject(kv));
        });

        webView.registerHandler("hideOptionMenu", (WVJBWebView.WVJBHandler<Object, JSONObject>) (o, callback) -> {
            menuArray = null;
            if (fragment != null) {
                fragment.getFragmentActivity().invalidateOptionsMenu();
            }

            Map<String, Object> kv = new HashMap<>();
            kv.put("status", true);
            callback.onResult(new JSONObject(kv));
        });

        webView.registerHandler("setTitle", (WVJBWebView.WVJBHandler<String, Object>) (str, callback) -> {
            if (fragment != null && fragment.getToolbarActivity() != null) {
                fragment.getToolbarActivity().setTopTitle(str);
            }
        });

        webView.registerHandler("uploadImage", this::upload);

        webView.registerHandler("setTitleBarColor", (WVJBWebView.WVJBHandler<String, Void>) (data, callback) -> {
            if (fragment != null && fragment.getToolbarActivity() != null
                    && fragment.getToolbarActivity().getToolbar() != null) {

                fragment.getToolbarActivity().getToolbar().setBackgroundColor(
                        Color.parseColor("#" + data));
            }
        });

        webView.registerHandler("getStatusBarHeight", (WVJBWebView.WVJBHandler<Object, Integer>) (data, callback) -> callback.onResult(getStatusBarHeight()));

        webView.registerHandler("QRScan", (WVJBWebView.WVJBHandler<JSONObject, JSONObject>) (o, callback) -> {
            Intent intent = new Intent(fragment.getFragmentActivity().getPackageName() + ".action.START_ACTIVITY");
            intent.putExtra("type", o.optString("scanType"));
            intent.putExtra("needResult", o.optString("needResult"));
            intent.addCategory("com.yuwell.qrscan");
            scanCallback = callback;
            if (fragment != null) {
                fragment.getFragmentActivity().startActivityForResult(intent, REQUEST_CODE_SCAN);
            }
        });

        webView.registerHandler("openWindow", (WVJBWebView.WVJBHandler<JSONObject, JSONObject>) (o, callback) -> {
            if (o != null) {
                Intent intent = new Intent();
                intent.setAction(fragment.getFragmentActivity().getPackageName() + ".action.START_ACTIVITY");
                intent.addCategory("com.yuwell." + o.optString("name"));

                JSONObject extra;
                if ((extra = o.optJSONObject("extras")) != null) {
                    Iterator<String> iterator = extra.keys();

                    String key;
                    while (iterator.hasNext()) {
                        key = iterator.next();
                        intent.putExtra(key, extra.optString(key));
                    }
                }

                Map<String, Object> kv = new HashMap<>();
                if (intent.resolveActivity(fragment.getFragmentActivity().getPackageManager()) != null) {
                    if (fragment != null) {
                        fragment.getFragmentActivity().startActivity(intent);
                    }
                    kv.put("status", true);
                } else {
                    kv.put("status", false);
                }
                callback.onResult(new JSONObject(kv));
            }
        });

        webView.registerHandler("setPullRefresh", (WVJBWebView.WVJBHandler<Boolean, Void>) (o, callback) -> {
            if (fragment != null) {
                fragment.setRefreshEnabled(o);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_FROM_LIBRARY) {
                String actualUri = ImageUtil.resolveImageUriToActual(fragment.getFragmentActivity(), data.getData());
                if (!TextUtils.isEmpty(actualUri)) {
                    displayImageOnWeb(actualUri);
                } else {
                    Toast.makeText(fragment.getFragmentActivity(), "Image not available", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == REQUEST_FROM_CAMERA) {
                displayImageOnWeb(ImageUtil.resolveImageUriToActual(fragment.getFragmentActivity(), mPhotoUri));
            } else if (requestCode == REQUEST_CODE_SCAN) {
                String result = data.getStringExtra("scan_result");
                if (scanCallback != null) {
                    Map<String, Object> kv = new HashMap<>();
                    kv.put("resultStr", result);
                    scanCallback.onResult(new JSONObject(kv));
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (menuArray != null) {
            for (int i = 0; i < menuArray.length(); i++) {
                JSONObject menuObj = menuArray.optJSONObject(i);
                MenuItem item = menu.add(Menu.NONE, i, Menu.NONE, menuObj.optString("name"));
                if (i == 0 && menuArray.length() == 1) {
                    item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        JSONObject menuObj;
        if (menuArray != null && (menuObj = menuArray.optJSONObject(item.getItemId())) != null) {
            if (fragment != null && fragment.getWVJBWebView() != null) {
                fragment.getWVJBWebView().callHandler(menuObj.optString("action"));
                return true;
            }
        }
        return false;
    }

    private void showChooseImgDialog(JSONArray array) {
        if (fragment == null)
            return;

        if (array == null || array.length() == 2) {
            AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getFragmentActivity());
            builder.setTitle("选择图片");
            builder.setItems(new String[]{"相册", "相机"}, (dialog, which) -> {
                if (which == 0) {
                    ImageUtil.chooseFromLib(fragment.getFragmentActivity(), REQUEST_FROM_LIBRARY);
                } else {
                    mPhotoUri = FileManager.getTempImageUri(fragment.getFragmentActivity(), "FileProvider1");
                    ImageUtil.takePhoto(fragment.getFragmentActivity(), REQUEST_FROM_CAMERA, mPhotoUri);
                }
            });
            builder.create().show();
        } else if (array.length() == 1) {
            if ("album".equalsIgnoreCase(array.optString(0))) {
                ImageUtil.chooseFromLib(fragment.getFragmentActivity(), REQUEST_FROM_LIBRARY);
            }
            if ("camera".equalsIgnoreCase(array.optString(0))) {
                mPhotoUri = FileManager.getTempImageUri(fragment.getFragmentActivity(), "");
                ImageUtil.takePhoto(fragment.getFragmentActivity(), REQUEST_FROM_CAMERA, mPhotoUri);
            }
        }
    }

    private void displayImageOnWeb(String actualUri) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageUtil.compress(byteArrayOutputStream, ImageUtil.getSmallBitmap(actualUri, 800, 800),80);
            String encoded = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            long id = System.currentTimeMillis() / 1000;
            idPathMap.put(id, actualUri);
            if (imageCallback != null) {
                Map<String, Object> kv = new HashMap<>();
                kv.put("data", "data:image/jpeg;base64," + encoded);
                kv.put("localId", id);
                imageCallback.onResult(new JSONObject(kv));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upload(JSONObject param, final WVJBWebView.WVJBResponseCallback<JSONObject> callback) {
        String filePath = idPathMap.get(param.optLong("localId"));

        if (!TextUtils.isEmpty(filePath)) {
            File file = new File(filePath);

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("image", file.getName(), RequestBody.create(MediaType.parse("image/jpg"), file))
                    .build();

            Request request = new Request.Builder()
                    .url(param.optString("url"))
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Map<String, Object> kv = new HashMap<>();
                    kv.put("status", false);
                    kv.put("error", "101");
                    callback.onResult(new JSONObject(kv));
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    Map<String, Object> kv = new HashMap<>();
                    if (response.isSuccessful() && response.body() != null) {
                        String retString = response.body().string();
                        Log.d(TAG, "onResponse: " + retString);
                        try {
                            JSONObject retJSON = new JSONObject(retString);
                            retJSON.put("status", true);
                            callback.onResult(retJSON);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        kv.put("status", false);
                        kv.put("error", "102");
                        callback.onResult(new JSONObject(kv));
                    }
                }
            });
        } else {
            Map<String, Object> kv = new HashMap<>();
            kv.put("status", false);
            kv.put("error", "103");
            callback.onResult(new JSONObject(kv));
        }
    }

    private int getStatusBarHeight() {
        Resources resources = fragment.getFragmentActivity().getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");

        int dp;
        if (resourceId > 0) {
            dp = resources.getDimensionPixelSize(resourceId);
        } else {
            dp = TypedValue.complexToDimensionPixelSize(24, fragment.getFragmentActivity()
                    .getResources().getDisplayMetrics());
        }
        return DensityUtil.px2dp(fragment.getFragmentActivity(), dp);
    }
}
