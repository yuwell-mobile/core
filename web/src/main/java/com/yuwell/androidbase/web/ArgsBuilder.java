package com.yuwell.androidbase.web;

import android.os.Bundle;

import java.util.HashMap;

/**
 * Created by Chen on 2019/12/4.
 */
public class ArgsBuilder {

    public static final String URL = "url";
    public static final String HEADERS = "headers";

    private Bundle args;

    public ArgsBuilder() {
        args = new Bundle();
    }

    public ArgsBuilder setUrl(String url) {
        args.putString(URL, url);
        return this;
    }

    public ArgsBuilder setTitle(int title) {
        args.putInt("title", title);
        return this;
    }

    public ArgsBuilder setHeader(HashMap<String, String> headers) {
        args.putSerializable(HEADERS, headers);
        return this;
    }

    public Bundle create() {
        return args;
    }
}
