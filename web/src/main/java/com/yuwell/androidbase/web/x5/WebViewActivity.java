package com.yuwell.androidbase.web.x5;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.yuwell.androidbase.view.ToolbarActivity;
import com.yuwell.androidbase.web.ArgsBuilder;
import com.yuwell.androidbase.web.R;
import com.yuwell.androidbase.web.manager.x5.WVJBFragment;

import wendu.webviewjavascriptbridge.x5.WVJBWebView;

/**
 * WebView Activity
 * Created by Chen on 2015/7/31.
 */
public class WebViewActivity extends ToolbarActivity {

    public static final String DATA_URL = "url";

    private WVJBFragment webViewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            webViewFragment = getWebViewFragment();

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame, (Fragment) webViewFragment, webViewFragment.getClass().getSimpleName());
            ft.commit();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_container;
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (webViewFragment.getHandlerManager().onCreateOptionsMenu(menu)) {
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (webViewFragment.getHandlerManager().onOptionsItemSelected(item)) {
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        webViewFragment.getHandlerManager().onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void goBack() {
        if (getWebView().canGoBack()) {
            getWebView().goBack();
        } else {
            finish();
        }
    }

    public WVJBWebView getWebView() {
        return webViewFragment.getWVJBWebView();
    }

    public void setRefreshEnabled(boolean enabled) {
        webViewFragment.setRefreshEnabled(enabled);
    }

    public WebViewFragment getWebViewFragment() {
        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(new ArgsBuilder().setUrl(getIntent().getStringExtra(DATA_URL)).create());
        return fragment;
    }

    public static void start(Context context, String url) {
        Intent starter = new Intent(context, WebViewActivity.class);
        starter.putExtra(DATA_URL, url);
        context.startActivity(starter);
    }

}
