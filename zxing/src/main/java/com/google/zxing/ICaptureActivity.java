package com.google.zxing;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;

import com.google.zxing.camera.CameraManager;

/**
 * Created by chenshuai on 2016/1/11.
 */
public interface ICaptureActivity {

    Handler getHandler();

    Rect getCropRect();

    CameraManager getCameraManager();

    void handleDecode(Result result, Bundle bundle);

    void setResult(int resultCode, Intent data);
}
