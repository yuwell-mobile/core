package com.google.zxing;

/**
 * Created by chenshuai on 2016/1/11.
 */
public interface Constants {

    int auto_focus = 0x10081001;
    int decode = 0x10081002;
    int decode_failed = 0x10081003;
    int decode_succeeded = 0x10081004;
    int encode_failed = 0x10081005;
    int encode_succeeded = 0x10081006;
    int launch_product_query = 0x10081007;
    int quit = 0x10081008;
    int restart_preview = 0x10081009;
    int return_scan_result = 0x10081010;
    int search_book_contents_failed = 0x10081011;
    int search_book_contents_succeeded = 0x10081012;
}
