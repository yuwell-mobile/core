package com.yuwell.androidbase.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.yuwell.androidbase.R;
import com.yuwell.androidbase.tool.Version;
import com.yuwell.androidbase.view.inter.InterToolbarActivity;

/**
 * 默认标题栏Activity
 * Created by Chen on 2015/3/23.
 */
public abstract class ToolbarActivity extends AppBaseActivity implements InterToolbarActivity {

    private Toolbar toolbar;
    private TextView mTitle;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setToolbar();
    }

    protected void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            if (showNavigation()) {
                toolbar.setNavigationIcon(getNavigationIcon());
            }
            toolbar.setTitle("");
            mTitle = toolbar.findViewById(R.id.text_title);
            mTitle.setText(getTitleText());
            setSupportActionBar(toolbar);

            if (Version.hasKitKat() && translucentStatus()) {
                setStatusBarTranslucent();
                toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
            }
        }
    }

    protected abstract int getLayoutId();

    protected int getTitleId() {
        return 0;
    }

    protected String getTitleText() {
        return getTitleId() == 0 ? null : getString(getTitleId());
    }

    protected boolean showNavigation() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void setTitle(CharSequence title) {
        if (mTitle != null) {
            mTitle.setText(title);
        }
    }

    @Override
    public void setTitle(int titleId) {
        if (mTitle != null) {
            mTitle.setText(titleId);
        }
    }

    @Override
    public void setTopTitle(String title) {
        setTitle(title);
    }

    public boolean translucentStatus() {
        return false;
    }

    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public final void setTitleVisibility(int visibility) {
        if (mTitle.getVisibility() != visibility) {
            mTitle.setVisibility(visibility);
        }
    }

    public int getNavigationIcon() {
        return R.drawable.ic_arrow_back_24dp;
    }
}
