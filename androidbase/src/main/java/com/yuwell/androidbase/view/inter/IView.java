package com.yuwell.androidbase.view.inter;

import com.yuwell.androidbase.presenter.AbstractPresenter;

/**
 * Created by Chen on 2016/1/11.
 */
public interface IView<T extends AbstractPresenter> {

    void setPresenter(T presenter);

    void showToast(int resId);

    void showToast(String msg);
}
