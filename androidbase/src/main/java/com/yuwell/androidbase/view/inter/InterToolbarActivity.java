package com.yuwell.androidbase.view.inter;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by Chen on 2019/12/4.
 */
public interface InterToolbarActivity {

    @Nullable
    Toolbar getToolbar();

    void setTopTitle(String title);
}
