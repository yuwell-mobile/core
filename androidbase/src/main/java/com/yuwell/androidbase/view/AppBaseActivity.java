package com.yuwell.androidbase.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import com.yuwell.androidbase.tool.ProgressDialogUtil;
import com.yuwell.androidbase.tool.ToastUtil;
import com.yuwell.androidbase.tool.Version;

/**
 * Base activity
 * Created by Chen on 2015/7/15.
 */
public abstract class AppBaseActivity extends AppCompatActivity {

    ProgressDialogUtil progressDialogUtil;
    ToastUtil toastUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialogUtil = new ProgressDialogUtil(this);
        toastUtil = new ToastUtil(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    /**
     * 显示查询提示框
     */
    protected void showProgressDialog(int resId) {
        progressDialogUtil.showProgressDialog(resId);
    }

    /**
     * 设置进度框提示
     * @param resId
     */
    protected void setProgressDialogMessage(int resId) {
        progressDialogUtil.setProgressDialogMessage(resId);
    }

    /**
     * 关闭查询提示框
     */
    public void dismissProgressDialog() {
        progressDialogUtil.dismissProgressDialog();
    }

    protected Intent getDefaultIntent(Class<?> cls) {
        return new Intent(this, cls);
    }

    public void showMessage(int msg) {
        toastUtil.showToast(msg);
    }

    public void showMessage(String msg) {
        toastUtil.showToast(msg);
    }

    protected void setStatusBarTranslucent() {
        if (Version.hasKitKat()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    /**
     * 关闭软键盘
     */
    protected void hideKeyboardForCurrentFocus() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null && inputMethodManager.isActive()) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

}
