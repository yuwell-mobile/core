package com.yuwell.androidbase.presenter;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.yuwell.androidbase.view.inter.IView;

/**
 * Super class for Presenters
 * Created by Chen on 2016/1/11.
 */
public abstract class AbstractPresenter<T extends IView> implements LifecycleObserver {

    private static final String TAG = AbstractPresenter.class.getSimpleName();

    public Context context;
    public T view;

    public AbstractPresenter(Context context, T view) {
        this.context = context;
        this.view = view;
        view.setPresenter(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onStart() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {

    }

    public void dealWithError(IView view, Throwable e) {
        Log.e(TAG, "dealWithError", e);
    }
}
