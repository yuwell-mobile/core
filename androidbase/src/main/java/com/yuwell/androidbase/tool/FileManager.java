package com.yuwell.androidbase.tool;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static android.content.ContentValues.TAG;

/**
 * Created by Chen on 2016/12/17.
 */

public class FileManager {

    private static final String IMAGE = "image";
    private static final String TEMP = "temp";

    public static Uri getTempImageUri(Context context, String providerName) {
        return getFileUri(context, FileManager.getUserImageDir(context), System.currentTimeMillis() + ".jpg", providerName);
    }

    public static Uri getFileUri(Context context, String path, String fileName, String providerName) {
        File file = new File(path, fileName);
        return getFileUri(context, file, providerName);
    }

    public static Uri getFileUri(Context context, File file, String providerName) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            return FileProvider.getUriForFile(context,
                    context.getPackageName() + "." + providerName, file);
        } else {
            return Uri.parse("file://" + file);
        }
    }

    public static String getUserImageDir(Context context) {
        String tmpDir = getTempDir(context);
        String imageDir = "";
        if (!TextUtils.isEmpty(tmpDir)) {
            imageDir = tmpDir + File.separator + IMAGE;
            return mkdirsIfNotExist(imageDir);
        } else {
            return imageDir;
        }
    }

    public static String getTempDir(Context context) {
        String path = getCacheDir(context) + File.separator + TEMP;
        return mkdirsIfNotExist(path);
    }

    public static String getLogDir(Context context) {
        return getDir(context, "LOG");
    }

    public static String getDir(Context context, String dir) {
        String pathName = getCacheDir(context) + File.separator + dir;
        return mkdirsIfNotExist(pathName);
    }

    private static String getCacheDir(Context context) {
        if (isExternalStorageMounted()) {
            File path = context.getExternalCacheDir();
            if (path != null) {
                return path.getAbsolutePath();
            }
        } else {
            File path = context.getCacheDir();
            if (path != null) {
                return path.getAbsolutePath();
            }
        }
        return "";
    }

    private static boolean isExternalStorageMounted() {
        boolean canRead = Environment.getExternalStorageDirectory().canRead();
        boolean onlyRead = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        boolean unMounted = Environment.getExternalStorageState().equals(Environment.MEDIA_UNMOUNTED);

        return !(!canRead || onlyRead || unMounted);
    }

    public static String mkdirsIfNotExist(String path) {
        File dir = new File(path);
        if (!dir.exists()) {
            if (dir.mkdirs()) {
                return path;
            } else {
                return "";
            }
        }
        return path;
    }

    public static String calculateMD5(File updateFile) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Exception while getting digest", e);
            return null;
        }

        InputStream is;
        try {
            is = new FileInputStream(updateFile);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Exception while getting FileInputStream", e);
            return null;
        }

        byte[] buffer = new byte[8192];
        int read;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Unable to process file for MD5", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(TAG, "Exception on closing MD5 input stream", e);
            }
        }
    }

    public static boolean copyFile(String from, String to) {
        return copyFile(new File(from), new File(to));
    }

    public static boolean copyFile(File fromFile, File toFile) {
        boolean flag = false;

        if (!fromFile.exists()) {
            throw new RuntimeException("File not exists!");
        }

        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(fromFile);
            fos = new FileOutputStream(toFile);

            byte[] buffer = new byte[1024 * 4];
            int length;

            while ((length = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, length);
            }

            flag = true;
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

    public static boolean copyFileUsingFileChannel(String from, String to) {
        return copyFileUsingFileChannel(new File(from), new File(to));
    }

    public static boolean copyFileUsingFileChannel(File from, File to) {
        boolean flag = false;
        FileInputStream fis = null;
        FileOutputStream fos = null;
        FileChannel fcIn = null;
        FileChannel fcOut = null;

        try {
            fis = new FileInputStream(from);
            fos = new FileOutputStream(to);
            fcIn = fis.getChannel();
            fcOut = fos.getChannel();

            fcIn.transferTo(0, fcIn.size(), fcOut);

            flag = true;
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            flag = false;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            flag = false;
        } finally {
            try {
                fcIn.close();
                fis.close();
                fcOut.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
                flag = false;
            }
        }
        return flag;
    }
}
