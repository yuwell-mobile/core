package com.yuwell.androidbase.tool;

import android.app.ProgressDialog;
import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Control ProgressDialog
 * Created by Chen on 2015/4/22.
 */
public class ProgressDialogUtil {

    protected ProgressDialog mDialog;
    protected WeakReference<Context> context;

    public ProgressDialogUtil(Context context) {
        this.context = new WeakReference<>(context);
    }

    public void showProgressDialog(int resId) {
        Context ctx = context.get();
        if (ctx != null) {
            if (mDialog == null) {
                mDialog = new ProgressDialog(ctx);
                mDialog.setMessage(ctx.getString(resId));
                mDialog.setCancelable(false);
                mDialog.setIndeterminate(true);
                mDialog.show();
            } else {
                setProgressDialogMessage(resId);
                if (!mDialog.isShowing()) {
                    mDialog.show();
                }
            }
        }
    }

    public void onDestroy() {
        dismissProgressDialog();
        context.clear();
    }

    /**
     * Set ProgressDialog message
     */
    public void setProgressDialogMessage(int resId) {
        Context ctx = context.get();
        if (ctx != null) {
            if (mDialog != null) {
                mDialog.setMessage(ctx.getString(resId));
            }
        }
    }

    public void setProgressDialogMessage(String text) {
        Context ctx = context.get();
        if (ctx != null && mDialog != null) {
            mDialog.setMessage(text);
        }
    }

    /**
     * Dismiss ProgressDialog
     */
    public void dismissProgressDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
