package com.yuwell.androidbase.tool;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * 功能描述：单位转换工具类
 * Created by chenshuai.
 */
public class DensityUtil {
	
    /**
     * 将单位为dip的值转换成单位为px的值
     * @param dipValue dip值
     * @return px值
     */
    public static int dip2px(Context c, float dipValue) {
        final float scale = getDisplayMetrics(c).density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 将单位为px的值转换成单位为dip的值
     * @param pxValue 像素值
     * @return dip值
     */
    public static int px2dip(Context c, float pxValue) {
        final float scale = getDisplayMetrics(c).density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     * @return
     */
    public static int px2sp(Context c, float pxValue) {
        final float scale = getDisplayMetrics(c).density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     * @param spValue
     * @return
     */
    public static int sp2px(Context c, float spValue) {
        final float scale = getDisplayMetrics(c).density;
        return (int) (spValue * scale + 0.5f);
    }
    
    /**
     * 将px值转化为dp值
     * @param pxValue
     * @return
     */
    public static int px2dp(Context c, float pxValue){
        final float scale = getDisplayMetrics(c).densityDpi;
        return (int) ((pxValue * 160) / scale + 0.5f);
    }
    
    private static DisplayMetrics getDisplayMetrics(Context ctx) {
    	return ctx.getResources().getDisplayMetrics();
    }

    public static int getWidthPixels(Context c) {
        return getDisplayMetrics(c).widthPixels;
    }

    public static int getHeightPixels(Context c) {
        return getDisplayMetrics(c).heightPixels;
    }

    public static float getDPI(Context c) {
        return getDisplayMetrics(c).density;
    }
}
