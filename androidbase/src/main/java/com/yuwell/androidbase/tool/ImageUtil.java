package com.yuwell.androidbase.tool;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Utils that handles image related work
 * Created by Chen on 2016/12/17.
 */

public class ImageUtil {

	public static Intent getImageContent(Context context) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");

		if (intent.resolveActivity(context.getPackageManager()) != null) {
			return intent;
		}

		return null;
	}

	public static Intent getImageCapture(Context context, Uri uri) {
		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

		if (intent.resolveActivity(context.getPackageManager()) != null) {
			return intent;
		}

		return intent;
	}

    public static boolean chooseFromLib(Activity activity, int cmd) {
        Intent intent = getImageContent(activity);
        if (intent != null) {
            activity.startActivityForResult(intent, cmd);
            return true;
        } else {
            return false;
        }
    }

    public static boolean chooseFromLib(Fragment f, int cmd) {
        Intent intent = getImageContent(f.getContext());
        if (intent != null) {
            f.startActivityForResult(intent, cmd);
            return true;
        } else {
            return false;
        }
    }

    public static boolean takePhoto(final Activity activity, final int cmd, Uri uri) {
		final Intent intent = getImageCapture(activity, uri);
        if (intent != null) {
            activity.startActivityForResult(intent, cmd);
            return true;
        } else {
            return false;
        }
    }

    public static boolean takePhoto(final Fragment f, final int cmd, Uri uri) {
        final Intent intent = getImageCapture(f.getContext(), uri);
        if (intent != null) {
            f.startActivityForResult(intent, cmd);
            return true;
        } else {
            return false;
        }
    }

    public static boolean compress(OutputStream os, Bitmap bitmap, int quality) {
        return bitmap.compress(Bitmap.CompressFormat.JPEG, quality, os);
    }

    public static File getCompressedFile(String pathToFile, String savePath, int reqWidth, int reqHeight) throws FileNotFoundException {
        File file = new File(pathToFile);
        File compressedFile = new File(savePath, "compress_" + file.getName());
        FileOutputStream fos = new FileOutputStream(compressedFile);
        if (compress(fos, getSmallBitmap(pathToFile, reqWidth, reqHeight), 80)) {
            return compressedFile;
        } else {
            return null;
        }
    }

    public static File getCompressedFile(String pathToFile, String savePath) throws FileNotFoundException {
        return getCompressedFile(pathToFile, savePath, 1440, 1440);
    }

    public static File getCompressedFile(String pathToFile, String savePath, float percent)
            throws FileNotFoundException {
        File file = new File(pathToFile);
        File compressedFile = new File(savePath, "compress_" + file.getName());
        FileOutputStream fos = new FileOutputStream(compressedFile);
        if (compress(fos, getSmallBitmap(pathToFile, percent), 80)) {
            return compressedFile;
        } else {
            return null;
        }
    }

    public static String resolveImageUriToActual(Context context, Uri imageUri) {
        return resolveImageUriToActual(context, imageUri, FileManager.getUserImageDir(context));
    }

    public static String resolveImageUriToActual(Context context, Uri imageUri, String savePath) {
        if (context == null || imageUri == null)
            return null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, imageUri)) {
            if (isExternalStorageDocument(imageUri)) {
                String docId = DocumentsContract.getDocumentId(imageUri);
                String[] split = docId.split(":");
                String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(imageUri)) {
                String id = DocumentsContract.getDocumentId(imageUri);
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            } else if (isMediaDocument(imageUri)) {
                String docId = DocumentsContract.getDocumentId(imageUri);
                String[] split = docId.split(":");
                String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                String selection = MediaStore.Images.Media._ID + "=?";
                String[] selectionArgs = new String[] { split[1] };
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } // MediaStore (and general)
        else if ("content".equalsIgnoreCase(imageUri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(imageUri)) {
                return imageUri.getLastPathSegment();
            } else if (imageUri.getAuthority().contains(context.getPackageName())) {
                // TODO Temp method to resolve file provider uri
                return savePath + File.separator + imageUri.getLastPathSegment();
            } else {
                return getDataColumn(context, imageUri, null, null);
            }
        }
        // File
        else if ("file".equalsIgnoreCase(imageUri.getScheme())) {
            return imageUri.getPath();
        }
        return null;
    }

    public static Bitmap getSmallBitmap(String filePath, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    public static Bitmap getSmallBitmap(String filePath, float percent) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        if (percent < 1) {
            // Calculate inSampleSize
            options.inSampleSize = Math.round(1 / percent);
        }

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    /**
     * 计算图片的缩放值
     *
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        String column = MediaStore.Images.Media.DATA;
        String[] projection = { column };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

}
