package com.yuwell.androidbase.tool;

import android.content.Context;

import androidx.annotation.AnimRes;
import androidx.annotation.ArrayRes;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;

/**
 * Created by Chen on 2015/4/22.
 */
public class ResourceUtil {

    private static int getIdentifier(Context ctx, String paramString, String defType) {
        if (ctx != null) {
            return ctx.getResources().getIdentifier(paramString, defType, ctx.getPackageName());
        } else {
            return 0;
        }
    }

    public static @LayoutRes int getLayoutId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "layout");
    }

    public static @StringRes int getStringId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "string");
    }

    public static @DrawableRes int getDrawableId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "drawable");
    }

    public static int getStyleId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "style");
    }

    public static @IdRes int getId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "id");
    }

    public static @ColorRes int getColorId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "color");
    }

    public static int getStyleableId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "styleable");
    }

    public static @AnimRes int getAnimId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "anim");
    }

    public static @ArrayRes int getArrayId(Context ctx, String paramString) {
        return getIdentifier(ctx, paramString, "array");
    }

    public static String getString(Context ctx, String paramString) {
        return ctx.getString(getStringId(ctx, paramString));
    }
}
