package com.yuwell.androidbase.tool;

import android.content.Context;
import android.widget.Toast;

import java.lang.ref.WeakReference;

/**
 * Created by Chen on 2015/4/22.
 */
public class ToastUtil {

    private Toast toast = null;
    private WeakReference<Context> applicationContext;

    public ToastUtil(Context context) {
        this.applicationContext = new WeakReference<>(context);
    }

    /**
     * 显示toast类
     * @param text
     */
    public void showToast(String text) {
        Toast toast = getToast(text, Toast.LENGTH_SHORT);
        if (toast != null) {
            toast.show();
        }
    }

    /**
     * 显示toast类
     * @param resId
     */
    public void showToast(int resId) {
        Toast toast = getToast(resId, Toast.LENGTH_SHORT);
        if (toast != null) {
            toast.show();
        }
    }

    private Toast getToast(int resId, int duration) {
        if (applicationContext != null) {
            if (toast == null) {
                toast = Toast.makeText(applicationContext.get(), resId, duration);
            } else {
                toast.setText(resId);
            }
        }
        return toast;
    }

    private Toast getToast(String text, int duration) {
        if (applicationContext.get() != null) {
            if (toast == null) {
                toast = Toast.makeText(applicationContext.get(), text, duration);
            } else {
                toast.setText(text);
            }
        }
        return toast;
    }
}

