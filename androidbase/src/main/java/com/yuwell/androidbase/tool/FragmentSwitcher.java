package com.yuwell.androidbase.tool;

import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * Controls switching between fragments
 * Created by Chen on 2016/9/27.
 */

public class FragmentSwitcher {

    private String fromTag;

    private FragmentManager fm;

    private int checkId = 0;

    public FragmentSwitcher(FragmentActivity activity, Bundle savedInstanceState, int defaultCheckId) {
        this.fm = activity.getSupportFragmentManager();
        if (savedInstanceState != null) {
            fromTag = savedInstanceState.getString("fromTag");
            this.checkId = savedInstanceState.getInt("check");
        } else {
            this.checkId = defaultCheckId;
        }
    }

    public void load(@IdRes int containerId, int checkId, Class<? extends Fragment> toClazz,
                     Bundle args) {
        String toTag = toClazz.getName() + "@" + checkId;
        this.checkId = checkId;

        Fragment to = getToFragment(toTag, toClazz, args);
        Fragment from = fm.findFragmentByTag(fromTag);

        if (to != from) {
            FragmentTransaction ft = fm.beginTransaction();

            if (from == null) {
                ft.add(containerId, to, toTag);
            } else {
                if (to.isAdded() || fm.findFragmentByTag(toTag) != null) {
                    ft.hide(from).show(to);
                } else {
                    ft.hide(from).add(containerId, to, toTag);
                }
            }

            fromTag = toTag;

            ft.commitAllowingStateLoss();
        }
    }

    private Fragment getToFragment(String tag, Class<? extends Fragment> clazz, Bundle args) {
        Fragment to = fm.findFragmentByTag(tag);
        if (to == null) {
            try {
                to = clazz.newInstance();
                if (to != null) {
                    to.setArguments(args);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return to;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("fromTag", fromTag);
        outState.putInt("check", checkId);
    }

    @Nullable
    public Fragment getCurrentFragment() {
        return fm.findFragmentByTag(fromTag);
    }

    public int getCheckId() {
        return checkId;
    }

    public void setCheckId(int checkId) {
        this.checkId = checkId;
    }
}
