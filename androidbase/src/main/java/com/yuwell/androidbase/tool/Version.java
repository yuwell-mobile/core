/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuwell.androidbase.tool;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;

/**
 * Class containing some static utility methods about API version.
 */
public class Version {

    private Version() {

    }

    /**
     * API level is higher than 19
     */
    public static boolean hasKitKat() {
        return hasSpecificVersion(Build.VERSION_CODES.KITKAT); // VERSION_CODES.KITKAT;
    }

    public static boolean hasLollipop() {
        return hasSpecificVersion(Build.VERSION_CODES.LOLLIPOP);
    }

    public static boolean hasMarshmallow() {
        return hasSpecificVersion(Build.VERSION_CODES.M);
    }

    public static boolean hasNougat() {
        return hasSpecificVersion(Build.VERSION_CODES.N);
    }

    public static boolean hasSpecificVersion(int versionCode) {
        return Build.VERSION.SDK_INT >= versionCode;
    }
}