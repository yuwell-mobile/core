package com.yuwell.sample;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.yuwell.androidbase.web.x5.WebViewActivity;

import wendu.webviewjavascriptbridge.x5.WVJBWebView;

/**
 * Created by Chen on 2018/2/6.
 */

public class MyWebView extends WebViewActivity {

    private static final String TAG = "MyWebView";

    public static void start(Context context, String url) {
        Intent starter = new Intent(context, MyWebView.class);
        starter.putExtra(DATA_URL, url);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WVJBWebView.setWebContentsDebuggingEnabled(true);
    }
}
