package com.yuwell.sample;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by Chen on 2019/7/23.
 */
public class MainFragment extends Fragment implements MainView {

	private static final String TAG = "MainFragment";

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		new MainPresenter(context, this);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate: ");
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView: ");
		return inflater.inflate(R.layout.fragment_main, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d(TAG, "onViewCreated: ");

		view.findViewById(R.id.button_web).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					MyWebView.start(getContext(), "file:///android_asset/test.html");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart: ");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume: ");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause: ");
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop: ");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy: ");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		Log.d(TAG, "onDestroyView: ");
	}

	@Override
	public void setPresenter(MainPresenter presenter) {
		getLifecycle().addObserver(presenter);
	}

	@Override
	public void showToast(int resId) {

	}

	@Override
	public void showToast(String msg) {

	}
}
