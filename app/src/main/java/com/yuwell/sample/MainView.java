package com.yuwell.sample;

import com.yuwell.androidbase.view.inter.IView;

/**
 * Created by Chen on 2019/7/23.
 */
public interface MainView extends IView<MainPresenter> {
}
