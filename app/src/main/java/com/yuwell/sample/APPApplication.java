package com.yuwell.sample;

import android.app.Application;

import com.yuwell.androidbase.web.x5.X5Initializer;

public class APPApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		//搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。
		X5Initializer.init(this);
	}

}
