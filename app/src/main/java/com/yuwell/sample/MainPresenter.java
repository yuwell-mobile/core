package com.yuwell.sample;

import android.content.Context;
import android.util.Log;

import com.yuwell.androidbase.presenter.AbstractPresenter;

/**
 * Created by Chen on 2019/7/23.
 */
public class MainPresenter extends AbstractPresenter<MainView> {

	private static final String TAG = "MainPresenter";

	public MainPresenter(Context context, MainView view) {
		super(context, view);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate: ");
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart: ");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume: ");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause: ");
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop: ");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy: ");
	}
}
